import model.TileType._
import model._
import scala.math.{hypot, Pi, abs}

final class MyStrategy extends Strategy {
  override def move(self: Car, world: World, game: Game, move: Move) {
    val nextTile = world.tilesXY(self.nextWaypointX)(self.nextWaypointY)
    val trackTileSize = game.trackTileSize
    var nextWaypointX: Double = (self.nextWaypointX + 0.5D) * trackTileSize
    var nextWaypointY: Double = (self.nextWaypointY + 0.5D) * trackTileSize
    val cornerTileOffset: Double = 0.32 * trackTileSize

    nextTile match {
      case LEFT_TOP_CORNER =>
        nextWaypointX += cornerTileOffset
        nextWaypointY += cornerTileOffset
        move.enginePower = 0.75
      case RIGHT_TOP_CORNER =>
        nextWaypointX -= cornerTileOffset
        nextWaypointY += cornerTileOffset
        move.enginePower = 0.75
      case LEFT_BOTTOM_CORNER =>
        nextWaypointX += cornerTileOffset
        nextWaypointY -= cornerTileOffset
        move.enginePower = 0.75
      case RIGHT_BOTTOM_CORNER =>
        nextWaypointX -= cornerTileOffset
        nextWaypointY -= cornerTileOffset
        move.enginePower = 0.75
      case VERTICAL =>
        nextWaypointX = self.x
        val k = if (nextWaypointY < self.y) -0.5 else 0.5
        nextWaypointY = self.y + k * trackTileSize
        move.enginePower = 1.0
        if (k < 0) {
          if (world.tilesXY(self.nextWaypointX)(self.nextWaypointY - 1) == VERTICAL &&
            world.tilesXY(self.nextWaypointX)(self.nextWaypointY - 2) == VERTICAL)
            move.useNitro = true
        } else {
          if (world.tilesXY(self.nextWaypointX)(self.nextWaypointY + 1) == VERTICAL &&
            world.tilesXY(self.nextWaypointX)(self.nextWaypointY + 2) == VERTICAL)
            move.useNitro = true
        }
      case HORIZONTAL =>
        nextWaypointY = self.y
        val k = if (nextWaypointX < self.x) -0.5 else 0.5
        nextWaypointX = self.x + k * trackTileSize
        move.enginePower = 1.0
        if (k < 0) {
          if (world.tilesXY(self.nextWaypointX - 1)(self.nextWaypointY) == HORIZONTAL &&
            world.tilesXY(self.nextWaypointX - 2)(self.nextWaypointY) == HORIZONTAL)
            move.useNitro = true
        } else {
          if (world.tilesXY(self.nextWaypointX + 1)(self.nextWaypointY) == HORIZONTAL &&
            world.tilesXY(self.nextWaypointX + 2)(self.nextWaypointY) == HORIZONTAL)
            move.useNitro = true
        }
      case CROSSROADS =>
        move.enginePower = 1.0
      case BOTTOM_HEADED_T =>
        if (abs(self.speedX) > abs(self.speedY)) {
          if (self.speedX > 0)
            nextWaypointX -= cornerTileOffset
          else
            nextWaypointX += cornerTileOffset
          move.enginePower = 0.9
        } else {
          nextWaypointY += cornerTileOffset
          move.enginePower = 0.6
        }
      case LEFT_HEADED_T =>
        if (abs(self.speedX) > abs(self.speedY)) {
          nextWaypointX -= cornerTileOffset
          move.enginePower = 0.6
        } else {
          if (self.speedY < 0)
            nextWaypointY += cornerTileOffset
          else
            nextWaypointY -= cornerTileOffset
          move.enginePower = 0.9
        }
      case RIGHT_HEADED_T =>
        if (abs(self.speedX) > abs(self.speedY)) {
          nextWaypointX += cornerTileOffset
          move.enginePower = 0.6
        } else {
          if (self.speedY < 0)
            nextWaypointY += cornerTileOffset
          else
            nextWaypointY -= cornerTileOffset
          move.enginePower = 0.9
        }
      case TOP_HEADED_T =>
        if (abs(self.speedX) > abs(self.speedY)) {
          if (self.speedX > 0)
            nextWaypointX -= cornerTileOffset
          else
            nextWaypointX += cornerTileOffset
          move.enginePower = 0.9
        } else {
          nextWaypointY -= cornerTileOffset
          move.enginePower = 0.6
        }
//      case _ => move.enginePower = 0.5
    }
    val angleToWaypoint: Double = self.angleTo(nextWaypointX, nextWaypointY)
    val speedModule: Double = hypot(self.speedX, self.speedY)
    move.wheelTurn = angleToWaypoint * 32.0D / Pi
    throwOilIfNeeded(self, world, game, move)
    if (speedModule * speedModule * abs(angleToWaypoint) > 2.5D * 2.5D * Pi) {
      move.brake = true
    }
  }

  def throwOilIfNeeded(self: Car, world: World, game: Game, move: Move) = {
    val allCars = world.cars
    val carsWithoutMy = allCars.filterNot(_.id == self.id)
    carsWithoutMy.foreach(c => {
      val angleto = self.angleTo(c)
      if (angleto > (3 / 4 * Pi) || angleto < (-3 / 4 * Pi)) {
        //val distto = self.distanceTo(c)
        //if (distto > 5 && distto < 20)
        move.spillOil = true
      }
    })
  }
}
